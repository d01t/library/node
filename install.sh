#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	apt-transport-https \
	ca-certificates \
	curl \
	dnsutils \
	git \
	jq \
	procps \
	rsync \
	wget \
	unzip \
	zip
# Purge unnecessary files
rm -rf /var/lib/apt/lists/*

npm -g i pm2

cat <<EOF > /usr/local/bin/entrypoint.sh
#!/bin/sh
if [ -n "\${HOST_USER_ID}" ]
then
	usermod -o -u "\${HOST_USER_ID}" node
	groupmod -o -g "\${HOST_GROUP_ID}" node
fi

set -e

if [ "\${1#-}" != "\${1}" ] || [ -z "\$(command -v "\${1}")" ]; then
  set -- node "\$@"
fi

exec "\$@"
EOF

chmod +x /usr/local/bin/entrypoint.sh
